cmake_minimum_required(VERSION 3.14.5)

project(imgui)


set(IMGUI_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/imgui.cpp
                  ${CMAKE_CURRENT_SOURCE_DIR}/imgui_demo.cpp
                  ${CMAKE_CURRENT_SOURCE_DIR}/imgui_draw.cpp
                  ${CMAKE_CURRENT_SOURCE_DIR}/imgui_impl_glfw.cpp
                  ${CMAKE_CURRENT_SOURCE_DIR}/imgui_impl_opengl3.cpp
                  ${CMAKE_CURRENT_SOURCE_DIR}/imgui_widgets.cpp)

add_library(${PROJECT_NAME} STATIC ${IMGUI_SOURCES})

target_link_libraries(${PROJECT_NAME} PUBLIC glfw glad)


if(UNIX)
    find_package(OpenGL REQUIRED)
    target_link_libraries(${PROJECT_NAME} PUBLIC GL)
endif()

target_compile_definitions(${PROJECT_NAME} PUBLIC IMGUI_IMPL_OPENGL_LOADER_GLAD)
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
