# Ocean
You can check demo video [here](https://youtu.be/Q2MSGj4LooY)
![demo_image](/uploads/181ccbdebd632f13ce54c7215430dd6f/Screenshot_20210814_124333.png)
# Build
Open terminal or GitBASH and enter
```bash
git clone https://gitlab.com/vladislav_v_01/water-shader.git
cd water-shader
git submodule update --init --recursive
mkdir build && cd build
cmake ..
make
./ocean
``` 

# References
- For ocean physics: [Fynn-Jorin Flügge's thesis](https://tore.tuhh.de/handle/11420/1439?locale=en), [dli/waves](https://github.com/dli/waves), [fynnfluegge/oreon-engine](https://github.com/fynnfluegge/oreon-engine.git), [achalpandeyy/OceanFFT](https://github.com/achalpandeyy/OceanFFT.git), [Realtime GPGPU FFT Ocean Water Simulation](https://tore.tuhh.de/handle/11420/1439)
