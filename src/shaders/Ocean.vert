#version 460 core

layout (location = 0) in vec3 VSInWorldPos;
layout (location = 1) in vec2 VSInTexCoord;

out vec3 FSInWorldPos;
out vec2 FSInTexCoord;

uniform mat4 PV;
uniform int OceanSize;

uniform sampler2D DisplacementMap;

void main()
{
        vec3 position = VSInWorldPos + texture(DisplacementMap, VSInTexCoord).rgb * (512.f/OceanSize);
        gl_Position = PV * vec4(position, 1.f);

        FSInWorldPos = position;
        FSInTexCoord = VSInTexCoord;
}
