#version 460 core

layout (location = 0) out vec4 FSOutColor;

in vec3 FSInWorldPos;
in vec2 FSInTexCoord;

uniform vec3 WorldCameraPos;
uniform vec3 SunDirection;
uniform int WireFrame;

uniform sampler2D NormalMap;

vec3 HDR(vec3 color, float exposure)
{
    return 1.0 - exp(-color * exposure);
}

void main()
{
    if ((WireFrame != 0))
    {
        FSOutColor = vec4(0.f, 0.f, 0.f, 1.f);
        return;
    }

    vec3 normal = texture(NormalMap, FSInTexCoord).xyz;
    
    vec3 viewDir = normalize(WorldCameraPos - FSInWorldPos);
    float fresnel = 0.02f + 0.98f * pow(1.f - dot(normal, viewDir), 5.f);
    
    vec3 skyColor = vec3(3.2f, 9.6f, 12.8f);
    vec3 oceanColor = vec3(0.004f, 0.016f, 0.047f);
    float exposure = 0.35f;
    
    vec3 sky = fresnel * skyColor;
    float diffuse = clamp(dot(normal, normalize(-SunDirection)), 0.f, 1.f);
    vec3 water = (1.f - fresnel) * oceanColor * skyColor * diffuse;
    
    vec3 color = sky + water;

    FSOutColor = vec4(HDR(color, exposure), 1.f);
}
