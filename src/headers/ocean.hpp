#ifndef OCEAN_HPP
#define OCEAN_HPP

#include "glad.h"

#include "glm/glm.hpp"

#include "GLFW/glfw3.h"

#include <memory>
#include <string>

#include "camera.hpp"
#include "shader.hpp"
#include "texture.hpp"

struct OceanSettings
{
    unsigned int Width               = 1280;
    unsigned int Height              = 720;
    std::string  WindowTitle         = "Ocean";
    bool         EnableCursor        = true;
    bool         EnableDebugCallback = false;
};

class Ocean
{
public:
    Ocean();
    ~Ocean();

    void Initialize();
    void Update();

    int Run();

protected:
    void OnWindowResize(const int width, const int height);
    void OnKeyPress(const int keyCode);
    void OnKeyRelease(const int keyCode);
    void OnMouseMove(const float xOffset, const float yOffset);
    void OnMouseScroll(const float verticalOffset);

    GLFWwindow*   Window;
    float         DeltaTime = 0.0f;
    OceanSettings Settings;

private:
    void InitializeBase();

    void GLFWFramebufferSizeCallback(GLFWwindow* window,
                                     const int   width,
                                     const int   height);
    void GLFWMouseCallback(GLFWwindow*  window,
                           const double xPos,
                           const double yPos);
    void GLFWScrollCallback(GLFWwindow*  window,
                            const double xOffset,
                            const double yOffset);
    void GLFWKeyCallback(GLFWwindow* widnow,
                         const int   key,
                         const int   scancode,
                         const int   action,
                         const int   mods);

    static void GLFWFramebufferSizeCallbackHelper(GLFWwindow* window,
                                                  int         width,
                                                  int         height);
    static void GLFWMouseCallbackHelper(GLFWwindow* window,
                                        double      x_pos,
                                        double      y_pos);
    static void GLFWScrollCallbackHelper(GLFWwindow* window,
                                         double      x_offset,
                                         double      y_offset);
    static void GLFWKeyCallbackHelper(
        GLFWwindow* window, int key, int scancode, int action, int mods);

private:
    unsigned int GridVBO;
    unsigned int GridIBO;
    unsigned int GridVAO;

    std::unique_ptr<Camera> UserCamera = nullptr;

    std::unique_ptr<Shader>    InitialSpectrumProgram = nullptr;
    std::unique_ptr<Texture2D> InitialSpectrumTexture = nullptr;

    bool                       IsPingPhase      = true;
    std::unique_ptr<Shader>    PhaseProgram     = nullptr;
    std::unique_ptr<Texture2D> PingPhaseTexture = nullptr;
    std::unique_ptr<Texture2D> PongPhaseTexture = nullptr;

    std::unique_ptr<Shader>    SpectrumProgram = nullptr;
    std::unique_ptr<Texture2D> SpectrumTexture = nullptr;

    std::unique_ptr<Shader>    FFTHorizontalProgram = nullptr;
    std::unique_ptr<Shader>    FFTVerticalProgram   = nullptr;
    std::unique_ptr<Texture2D> TempTexture          = nullptr;

    std::unique_ptr<Shader>    NormalMapProgram = nullptr;
    std::unique_ptr<Texture2D> NormalMap        = nullptr;

    std::unique_ptr<Shader> OceanProgram = nullptr;

    bool ShowDebugGui = true;
    bool Changed      = true;
    bool FirstTime    = true;

    int OceanSize = 1024;

    //    float WindMagnitude = 14.142135f;
    float WindMagnitude = 15.0f;
    float WindAngle     = 45.f;
    float Choppiness    = 1.5f;
    int   SunElevation  = 0;
    int   SunAzimuth    = 90;
    bool  WireframeMode = false;

    float LastX = 0.f;
    float LastY = 0.f;

    struct GridVertex
    {
        glm::vec3 Position;
        glm::vec2 TextureCoords;
    };
};

#endif
