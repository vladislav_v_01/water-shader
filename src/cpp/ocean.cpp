#include "ocean.hpp"

#include <cstddef>
#include <random>
#include <vector>

#include "glm/gtc/type_ptr.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#define GRID_DIM 1024
#define RESOLUTION 512
#define WORK_GROUP_DIM 32

#define PI 3.14159265359f

Ocean::Ocean()
{
    Settings.Width               = 1280;
    Settings.Height              = 720;
    Settings.WindowTitle         = "Ocean";
    Settings.EnableCursor        = true;
    Settings.EnableDebugCallback = false;
}

Ocean::~Ocean() {}

void Ocean::Initialize()
{
    const int vertexCount = GRID_DIM + 1;

    // размер сетки
    std::vector<GridVertex> vertices(vertexCount * vertexCount);
    // размер сетки * 2 * 3 для треугольников
    std::vector<unsigned int> indices(GRID_DIM * GRID_DIM * 2 * 3);

    float textureCoordScale = 2.0f;

    // заполняем сетку вершинами и к каждой вершине привязываем текстурные
    // координаты
    unsigned int idx = 0;
    for (int z = -GRID_DIM / 2; z <= GRID_DIM / 2; ++z)
    {
        for (int x = -GRID_DIM / 2; x <= GRID_DIM / 2; ++x)
        {
            vertices[idx].Position = glm::vec3(float(x), 0.0f, float(z));

            float u                       = ((float)x / GRID_DIM) + 0.5f;
            float v                       = ((float)z / GRID_DIM) + 0.5f;
            vertices[idx++].TextureCoords = glm::vec2(u, v) * textureCoordScale;
        }
    }
    assert(idx == vertices.size());

    // заполняем массив индексов вершин треугольников
    idx = 0;
    for (unsigned int y = 0; y < GRID_DIM; ++y)
    {
        for (unsigned int x = 0; x < GRID_DIM; ++x)
        {
            indices[idx++] = (vertexCount * y) + x;
            indices[idx++] = (vertexCount * (y + 1)) + x;
            indices[idx++] = (vertexCount * y) + x + 1;

            indices[idx++] = (vertexCount * y) + x + 1;
            indices[idx++] = (vertexCount * (y + 1)) + x;
            indices[idx++] = (vertexCount * (y + 1)) + x + 1;
        }
    }
    assert(idx == indices.size());

    // GridVBO (привязываем к буфферу вершины сетки и координаты текстуры)
    glGenBuffers(1, &GridVBO);
    glBindBuffer(GL_ARRAY_BUFFER, GridVBO);
    glBufferData(GL_ARRAY_BUFFER,
                 vertices.size() * sizeof(GridVertex),
                 vertices.data(),
                 GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // GridIBO (привязываем к буфферу индексы треугольников)
    glGenBuffers(1, &GridIBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GridIBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 indices.size() * sizeof(unsigned int),
                 indices.data(),
                 GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // GridVAO (говорим буфферу, как лежат данныев памяти)
    glGenVertexArrays(1, &GridVAO);
    glBindVertexArray(GridVAO);
    glBindBuffer(GL_ARRAY_BUFFER, GridVBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GridIBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GridVertex), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1,
                          2,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(GridVertex),
                          (const GLvoid*)offsetof(GridVertex, TextureCoords));
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, GridVBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GridIBO);
    glBindVertexArray(0);

    // создаём объект камеры
    UserCamera = std::make_unique<Camera>(
        glm::vec3(0.f, 60.f, 0.f), 0.1f, 1000.f, 1000.f);

    // создаём программу для расчёта спектра на основе вычислительного шейдера
    // создаём текстуру для хранения значений вычислительного шейдера
    InitialSpectrumProgram =
        std::make_unique<Shader>("shaders/InitialSpectrum.comp");
    InitialSpectrumTexture = std::make_unique<Texture2D>(
        RESOLUTION, RESOLUTION, GL_R32F, GL_RED, GL_FLOAT);

    std::vector<float> pingPhaseArray(RESOLUTION * RESOLUTION);

    std::random_device                    dev;
    std::mt19937                          rng(dev());
    std::uniform_real_distribution<float> dist(0.0f, 1.0f);

    for (size_t i = 0; i < RESOLUTION * RESOLUTION; ++i)
    {
        pingPhaseArray[i] = dist(rng) * 2.0f * PI;
    }

    PhaseProgram     = std::make_unique<Shader>("shaders/Phase.comp");
    PingPhaseTexture = std::make_unique<Texture2D>(RESOLUTION,
                                                   RESOLUTION,
                                                   GL_R32F,
                                                   GL_RED,
                                                   GL_FLOAT,
                                                   GL_NEAREST,
                                                   GL_NEAREST,
                                                   GL_CLAMP_TO_BORDER,
                                                   GL_CLAMP_TO_BORDER,
                                                   pingPhaseArray.data());
    PongPhaseTexture = std::make_unique<Texture2D>(
        RESOLUTION, RESOLUTION, GL_R32F, GL_RED, GL_FLOAT);

    // спектр, который будет меняться каждые дельта
    SpectrumProgram =
        std::make_unique<Shader>("shaders/Spectrum.comp");
    SpectrumTexture = std::make_unique<Texture2D>(RESOLUTION,
                                                  RESOLUTION,
                                                  GL_RGBA32F,
                                                  GL_RGBA,
                                                  GL_FLOAT,
                                                  GL_LINEAR,
                                                  GL_LINEAR,
                                                  GL_REPEAT,
                                                  GL_REPEAT);
    // Временная текстура в которой будут хранится данные после итераций в
    // быстром преобразовании Фурье
    TempTexture = std::make_unique<Texture2D>(
        RESOLUTION, RESOLUTION, GL_RGBA32F, GL_RGBA, GL_FLOAT);

    // Преобразование Фурье
    FFTHorizontalProgram =
        std::make_unique<Shader>("shaders/FFTHorizontal.comp");
    FFTVerticalProgram =
        std::make_unique<Shader>("shaders/FFTVertical.comp");

    // карта нормалей
    NormalMapProgram =
        std::make_unique<Shader>("shaders/NormalMap.comp");
    NormalMap = std::make_unique<Texture2D>(RESOLUTION,
                                            RESOLUTION,
                                            GL_RGBA32F,
                                            GL_RGBA,
                                            GL_FLOAT,
                                            GL_LINEAR,
                                            GL_LINEAR,
                                            GL_REPEAT,
                                            GL_REPEAT);

    // шедер для отрисовки результатов
    OceanProgram = std::make_unique<Shader>("shaders/Ocean.vert",
                                            "shaders/Ocean.frag");

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glDisable(GL_BLEND);
    glClearColor(0.674f, 0.966f, 0.988f, 1.f);
}

void Ocean::Update()
{
    // ImGui
    if (ShowDebugGui)
    {
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
                    1000.0f / ImGui::GetIO().Framerate,
                    ImGui::GetIO().Framerate);

        // Примечание: только эти два параметра влияют на генерацию начального
        // спектра
        bool windMagChanged =
            ImGui::SliderFloat("Wind Magnitude", &WindMagnitude, 10.f, 50.f);
        bool windDirChanged =
            ImGui::SliderFloat("Wind Angle", &WindAngle, 0, 359);

        ImGui::SliderFloat("Choppiness", &Choppiness, 0.f, 2.5f);
        ImGui::SliderInt("Sun Elevation", &SunElevation, 0, 89);
        ImGui::SliderInt("Sun Azimuth", &SunAzimuth, 0, 359);
        ImGui::Checkbox("Wireframe", &WireframeMode);
        bool oceanSizeChanged =
            ImGui::SliderInt("Ocean Size", &OceanSize, 10, 30'000);

        Changed = windMagChanged || windDirChanged || oceanSizeChanged;
    }

    // Если два параметра влияющих на генерацию волн были изменены, тогда
    // генерируется новый начальный спектр, который будет использоваться в
    // дальнейших расчётах
    if (Changed || FirstTime)
    {
        InitialSpectrumProgram->use();
        // настройка юниформ шейдера для генерации начального спектра волн
        InitialSpectrumProgram->setInt("OceanSize", OceanSize);
        InitialSpectrumProgram->setInt("Resolution", RESOLUTION);

        float wind_angle_rad = glm::radians(WindAngle);
        InitialSpectrumProgram->setVec2(
            "Wind",
            WindMagnitude * glm::cos(wind_angle_rad),
            WindMagnitude * glm::sin(wind_angle_rad));
        InitialSpectrumTexture->BindImage(
            0, GL_WRITE_ONLY, InitialSpectrumTexture->get_InternalFormat());

        glDispatchCompute(
            RESOLUTION / WORK_GROUP_DIM, RESOLUTION / WORK_GROUP_DIM, 1);
        glFinish();

        Changed   = false;
        FirstTime = false;
    }

    PhaseProgram->use();

    // настройка юниформ для расчёта фаз волн
    PhaseProgram->setInt("OceanSize", OceanSize);
    PhaseProgram->setFloat("DeltaTime", DeltaTime);
    PhaseProgram->setInt("Resolution", RESOLUTION);

    // выбираем текстуру для чтения и записи данных о фазе
    if (IsPingPhase)
    {
        PingPhaseTexture->BindImage(
            0, GL_READ_ONLY, PingPhaseTexture->get_InternalFormat());
        PongPhaseTexture->BindImage(
            1, GL_WRITE_ONLY, PongPhaseTexture->get_InternalFormat());
    }
    else
    {
        PingPhaseTexture->BindImage(
            1, GL_WRITE_ONLY, PingPhaseTexture->get_InternalFormat());
        PongPhaseTexture->BindImage(
            0, GL_READ_ONLY, PongPhaseTexture->get_InternalFormat());
    }

    glDispatchCompute(
        RESOLUTION / WORK_GROUP_DIM, RESOLUTION / WORK_GROUP_DIM, 1);
    glFinish();

    // расчёт нового спектра
    SpectrumProgram->use();

    // настройка юниформ для шейдера расчёта формы волн спектральным методом
    SpectrumProgram->setInt("OceanSize", OceanSize);
    SpectrumProgram->setFloat("Choppiness", Choppiness);

    IsPingPhase ? PongPhaseTexture->BindImage(
                      0, GL_READ_ONLY, PongPhaseTexture->get_InternalFormat())
                : PingPhaseTexture->BindImage(
                      0, GL_READ_ONLY, PingPhaseTexture->get_InternalFormat());
    InitialSpectrumTexture->BindImage(
        1, GL_READ_ONLY, InitialSpectrumTexture->get_InternalFormat());

    SpectrumTexture->BindImage(
        2, GL_WRITE_ONLY, SpectrumTexture->get_InternalFormat());

    glDispatchCompute(
        RESOLUTION / WORK_GROUP_DIM, RESOLUTION / WORK_GROUP_DIM, 1);
    glFinish();

    // расчёт быстрого обратного преобразования Фурье
    FFTHorizontalProgram->use();
    // найстройка юниформ шейдера БПФ по строкам
    FFTHorizontalProgram->setInt("TotalCount", RESOLUTION);

    bool temp_as_input =
        false; // использовать ли временную текстуру как входные данные для БПФ

    // выполняем БПФ по строкам
    for (int p = 1; p < RESOLUTION; p <<= 1)
    {
        if (temp_as_input)
        {
            TempTexture->BindImage(
                0, GL_READ_ONLY, TempTexture->get_InternalFormat());
            SpectrumTexture->BindImage(
                1, GL_WRITE_ONLY, SpectrumTexture->get_InternalFormat());
        }
        else
        {
            SpectrumTexture->BindImage(
                0, GL_READ_ONLY, SpectrumTexture->get_InternalFormat());
            TempTexture->BindImage(
                1, GL_WRITE_ONLY, TempTexture->get_InternalFormat());
        }

        FFTHorizontalProgram->setInt("SubseqCount", p);

        // Одна группа на строку
        glDispatchCompute(RESOLUTION, 1, 1);
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

        temp_as_input = !temp_as_input;
    }

    FFTVerticalProgram->use();
    // найстройка юниформ шейдера БПФ по столбцам
    FFTVerticalProgram->setInt("TotalCount", RESOLUTION);

    // выполняем БПФ по столбцам
    for (int p = 1; p < RESOLUTION; p <<= 1)
    {
        if (temp_as_input)
        {
            TempTexture->BindImage(
                0, GL_READ_ONLY, TempTexture->get_InternalFormat());
            SpectrumTexture->BindImage(
                1, GL_WRITE_ONLY, SpectrumTexture->get_InternalFormat());
        }
        else
        {
            SpectrumTexture->BindImage(
                0, GL_READ_ONLY, SpectrumTexture->get_InternalFormat());
            TempTexture->BindImage(
                1, GL_WRITE_ONLY, TempTexture->get_InternalFormat());
        }

        FFTVerticalProgram->setInt("SubseqCount", p);

        // одна группа на столбец
        glDispatchCompute(RESOLUTION, 1, 1);
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

        temp_as_input = !temp_as_input;
    }

    // расчёт карты нормалей
    NormalMapProgram->use();
    SpectrumTexture->BindImage(
        0, GL_READ_ONLY, SpectrumTexture->get_InternalFormat());
    NormalMap->BindImage(1, GL_WRITE_ONLY, NormalMap->get_InternalFormat());

    // настройка юниформ для шейдера карты нормалей
    NormalMapProgram->setInt("Resolution", RESOLUTION);
    NormalMapProgram->setInt("OceanSize", OceanSize);

    glDispatchCompute(
        RESOLUTION / WORK_GROUP_DIM, RESOLUTION / WORK_GROUP_DIM, 1);
    glFinish();

    // отрисовка океана
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // настройка юниформ в шейдерах отрисовки океана
    OceanProgram->use();
    OceanProgram->setMat4(
        "PV",
        UserCamera->GetProjViewMatrix((float)Settings.Width / Settings.Height));
    OceanProgram->setVec3("WorldCameraPos",
                          UserCamera->Position.x,
                          UserCamera->Position.y,
                          UserCamera->Position.z);
    OceanProgram->setInt("OceanSize", OceanSize);
    OceanProgram->setInt("Wireframe", WireframeMode ? 1 : 0);

    float sun_elevation_rad = glm::radians((float)SunElevation);
    float sun_azimuth_rad   = glm::radians((float)SunAzimuth);
    OceanProgram->setVec3(
        "SunDirection",
        -glm::cos(sun_elevation_rad) * glm::cos(sun_azimuth_rad),
        -glm::sin(sun_elevation_rad),
        -glm::cos(sun_elevation_rad) * glm::sin(sun_azimuth_rad));

    OceanProgram->setInt("DisplacementMap", 0);
    temp_as_input
        ? TempTexture->Bind(0)
        : SpectrumTexture->Bind(
              0); // Todo: Make it clear that here we are binding disp map

    OceanProgram->setInt("NormalMap", 1);
    NormalMap->Bind(1);

    IsPingPhase = !IsPingPhase;

    glBindVertexArray(GridVAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GridIBO);

    // отрисовка линиями или треугольниками
    if (WireframeMode)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glDrawElements(
        GL_TRIANGLES, GRID_DIM * GRID_DIM * 2 * 3, GL_UNSIGNED_INT, 0);
}

void Ocean::OnWindowResize(const int width, const int height) {}

void Ocean::OnKeyPress(int key_code)
{
    // Todo:
    // 1. I would like the client to not touch GLFW code.
    // 2. This destroys the coherence between settings.enable_cursor and the
    // actual state of the cursor, fix this.

    if (key_code == GLFW_KEY_G)
    {
        if (!ShowDebugGui)
        {
            glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            ShowDebugGui = true;
        }
        else
        {
            glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            ShowDebugGui = false;
        }
    }

    if (!ShowDebugGui)
        UserCamera->ProcessKeyboard(key_code, DeltaTime);
}

void Ocean::OnKeyRelease(const int keyCode) {}

void Ocean::OnMouseMove(float x_offset, float y_offset)
{
    if (!ShowDebugGui)
        UserCamera->ProcessMouseMove(x_offset, y_offset);
}

void Ocean::OnMouseScroll(const float verticalOffset)
{
    if (!ShowDebugGui)
        UserCamera->ProcessMouseScroll(verticalOffset);
}

void Ocean::InitializeBase()
{
    if (glfwInit() != GLFW_TRUE)
        exit(-1);

    if (Settings.EnableDebugCallback)
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

    Window = glfwCreateWindow(Settings.Width,
                              Settings.Height,
                              Settings.WindowTitle.c_str(),
                              nullptr,
                              nullptr);
    if (!Window)
    {
        glfwTerminate();
        exit(-1);
    }

    // Настройка функций для обработки ввода с клавиатуры и мыши
    glfwSetFramebufferSizeCallback(Window, GLFWFramebufferSizeCallbackHelper);
    glfwSetKeyCallback(Window, GLFWKeyCallbackHelper);
    glfwSetCursorPosCallback(Window, GLFWMouseCallbackHelper);
    glfwSetScrollCallback(Window, GLFWScrollCallbackHelper);

    // вкл/выкл курсора
    glfwSetInputMode(Window,
                     GLFW_CURSOR,
                     Settings.EnableCursor ? GLFW_CURSOR_NORMAL
                                           : GLFW_CURSOR_DISABLED);

    glfwSetWindowUserPointer(Window, this);
    glfwMakeContextCurrent(Window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        exit(-1);
    }

    // Инитиализация ImGui
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(Window, true);
    ImGui_ImplOpenGL3_Init();

    // Todo: сделать логирование опциональным
    {
        const GLubyte* opengl_vendor   = glGetString(GL_VENDOR);
        const GLubyte* opengl_renderer = glGetString(GL_RENDERER);
        const GLubyte* opengl_version  = glGetString(GL_VERSION);

        std::cout << "GPU Vendor: " << opengl_vendor << std::endl;
        std::cout << "Renderer: " << opengl_renderer << std::endl;
        std::cout << "OpenGL Version: " << opengl_version << std::endl;

        std::cout << std::endl;

        GLint max_work_group_count;
        GLint max_work_group_size;
        for (unsigned int i = 0; i < 3; ++i)
        {
            glGetIntegeri_v(
                GL_MAX_COMPUTE_WORK_GROUP_COUNT, i, &max_work_group_count);
            glGetIntegeri_v(
                GL_MAX_COMPUTE_WORK_GROUP_SIZE, i, &max_work_group_size);
            std::cout << (char)(i + 88) << ":" << std::endl;
            std::cout << "\tMax Work Group Count: " << max_work_group_count
                      << std::endl;
            std::cout << "\tMax Work Group Size: " << max_work_group_size
                      << std::endl;

            std::cout << std::endl;
        }

        GLint max_work_group_invocations;
        glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS,
                      &max_work_group_invocations);

        std::cout << "Max Work Group Invocations: "
                  << max_work_group_invocations << std::endl;
    }

    // изменение позиций мыши
    LastX = Settings.Width / 2.f;
    LastY = Settings.Height / 2.f;
}

void Ocean::GLFWFramebufferSizeCallbackHelper(GLFWwindow* window,
                                              int         width,
                                              int         height)
{
    Ocean* app = (Ocean*)glfwGetWindowUserPointer(window);
    app->GLFWFramebufferSizeCallback(window, width, height);
}

void Ocean::GLFWMouseCallbackHelper(GLFWwindow* window,
                                    double      x_pos,
                                    double      y_pos)
{
    Ocean* app = (Ocean*)glfwGetWindowUserPointer(window);
    app->GLFWMouseCallback(window, x_pos, y_pos);
}

void Ocean::GLFWScrollCallbackHelper(GLFWwindow* window,
                                     double      x_offset,
                                     double      y_offset)
{
    Ocean* app = (Ocean*)glfwGetWindowUserPointer(window);
    app->GLFWScrollCallback(window, x_offset, y_offset);
}

void Ocean::GLFWKeyCallbackHelper(
    GLFWwindow* window, int key, int scancode, int action, int mods)
{
    Ocean* app = (Ocean*)glfwGetWindowUserPointer(window);
    app->GLFWKeyCallback(window, key, scancode, action, mods);
}

int Ocean::Run()
{
    InitializeBase();

    Initialize();

    while (!glfwWindowShouldClose(Window))
    {
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        float start_time = (float)glfwGetTime();

        Update();

        DeltaTime = (float)glfwGetTime() - start_time;

        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(Window);
        glfwPollEvents();
    }

    return 0;
}

void Ocean::GLFWFramebufferSizeCallback(GLFWwindow* window,
                                        const int   width,
                                        const int   height)
{
    glViewport(0, 0, width, height);
    OnWindowResize(width, height);
}

void Ocean::GLFWMouseCallback(GLFWwindow*  window,
                              const double x,
                              const double y)
{
    OnMouseMove(float(x - LastX), float(LastY - y));
    LastX = (float)x;
    LastY = (float)y;
}

void Ocean::GLFWScrollCallback(GLFWwindow*  window,
                               const double x_offset,
                               const double y_offset)
{
    OnMouseScroll((float)y_offset);
}

void Ocean::GLFWKeyCallback(GLFWwindow* window,
                            const int   key,
                            const int   scancode,
                            const int   action,
                            const int   mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

    if (action == GLFW_PRESS || action == GLFW_REPEAT)
        OnKeyPress(key);
    else if (action == GLFW_RELEASE)
        OnKeyRelease(key);
}
